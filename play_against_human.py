# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 13:17:29 2021

@author: Stoyan Boyukliyski
"""

import pandas as pd
import bespoke_functions as bspf
import core_game as core

header_list = ['is_point', 'total_count', 'win_rate', 'player_name']

'''

Choose between the two methods. 

The first method only takes into account wether the game ends in a draw or 
a win and treats the draw and it doesn't make a distinction between the two
and it also doesn't make a distinction at what tern the game ends.

The second should be an improvement but it serves as a scoring system rather 
than working with percentages. However, it gives lower priority to draws and 
and it also includes a small factor (0.05, but could vary). This encourages
the algorithm to finish games in the most direct manner.

'''


#dataset = pd.read_csv('statistics_moves.csv', names = header_list)

dataset = pd.read_csv('statistics_moves_alter_cost.csv', names = header_list)

core.show_moves()

board = {1: ' ', 2: ' ', 3: ' ',
         4: ' ', 5: ' ', 6: ' ',
         7: ' ', 8: ' ', 9: ' '}

priority = core.priority_function()

if priority == 'Yes':
    player, bot = ('X', 'O')
else:
    player, bot = ('O', 'X')
    
    
game = core.TicTacToe(board, player, bot)

game.print_board()

while not game.check_for_draw():
    if priority == 'Yes':
        game.player_move()
        computer_choice = bspf.computer_move_probabilistic(board, dataset, bot)
        game.make_a_move(bot, computer_choice)
        print(game.board)
    else:
        computer_choice = bspf.computer_move_probabilistic(board, dataset, bot)
        game.make_a_move(bot, computer_choice)
        game.player_move()
        print(game.board)
    