# -*- coding: utf-8 -*-
"""
Created on Tue Oct 26 23:21:14 2021

@author: Stoyan Boyukliyski
"""

import pandas as pd
import bespoke_functions as bspf


'''

Training stage for the cost based algorithm.

'''

board_positions = []
board_naming = []
points = []
player = []
        
for i in range(1000000):
    board_positions, board_naming, points, player = bspf.game_of_tic_tac_toe_altered(board_positions, board_naming, points, player)

moves = pd.DataFrame({'player_name' : player, 'is_point' : points, 'board' : board_naming})
moves.to_csv('stored_moves_cost_alter.csv', mode='a', header=False)


header_list = ['player_name', 'is_point', 'board']
moves = pd.read_csv('stored_moves_cost_alter.csv', names = header_list)

dataset = moves.groupby('board').sum()

dataset['total_count'] = moves.groupby('board').size()

dataset['win_rate'] = dataset['is_point']/moves.groupby('board').size()

dataset['player_name'] = moves.groupby('board').first()['player_name']

dataset.to_csv('statistics_moves_alter_cost.csv', header=False)