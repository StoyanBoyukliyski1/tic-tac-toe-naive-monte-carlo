# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 12:28:33 2021

@author: Stoyan Boyukliyski
"""

import numpy as np 

def check_for_win(board, mark):
    if board[0] == board[1] and board[0] == board[2] and board[0] == mark:
        return True
    elif (board[3] == board[4] and board[3] == board[5] and board[3] == mark):
        return True
    elif (board[6] == board[7] and board[6] == board[8] and board[6] == mark):
        return True
    elif (board[0] == board[3] and board[0] == board[6] and board[0] == mark):
        return True
    elif (board[1] == board[4] and board[1] == board[7] and board[1] == mark):
        return True
    elif (board[2] == board[5] and board[2] == board[8] and board[2] == mark):
        return True
    elif (board[0] == board[4] and board[0] == board[7] and board[0] == mark):
        return True
    elif (board[6] == board[4] and board[6] == board[2] and board[6] == mark):
        return True
    else:
        return False
        
def check_for_draw(board):
    for i in board:
        if i == ' ':
            return False
    return True

def computer_move_x(list_forward, board):
    board = board.copy()
    choice = np.random.choice(list_forward)
    choice_index = list_forward.index(choice)
    list_forward.pop(choice_index)
    board[choice] = 'X'
    return list_forward, board

def computer_move_o(list_forward, board):
    board = board.copy()
    choice = np.random.choice(list_forward)
    choice_index = list_forward.index(choice)
    list_forward.pop(choice_index)
    board[choice] = 'O'
    return list_forward, board


def game_of_tic_tac_toe(board_positions, board_naming, points, player):
    list_board = [i for i in range(9)]
    board = [' ' for i in range(9)]
    while len(list_board) != 0:
        list_board, board = computer_move_x(list_board, board)
        board_positions.append(board)
        board_naming.append(''.join(board))
        if check_for_win(board, 'X'):
            points.extend([np.abs(1-np.mod(i,2)) for i in range(9-len(list_board))])
            player.extend(['X' if np.mod(i, 2) == 0 else 'O' for i in range(9-len(list_board))])
            return board_positions, board_naming, points, player
        elif check_for_draw(board):
            points.extend([1 for i in range(9-len(list_board))])
            player.extend(['X' if np.mod(i, 2) == 0 else 'O' for i in range(9-len(list_board))])
            return board_positions, board_naming, points, player
        
        list_board, board = computer_move_o(list_board, board)
        board_positions.append(board)
        board_naming.append(''.join(board))
        if check_for_win(board, 'O'):
            points.extend([np.mod(i,2) for i in range(9-len(list_board))])
            player.extend(['X' if np.mod(i, 2) == 0 else 'O' for i in range(9-len(list_board))])
            return board_positions, board_naming, points, player
        elif check_for_draw(board):
            points.extend([1 for i in range(9-len(list_board))])
            player.extend(['X' if np.mod(i, 2) == 0 else 'O' for i in range(9-len(list_board))])
            return board_positions, board_naming, points, player
        
def game_of_tic_tac_toe_altered(board_positions, board_naming, points, player):
    list_board = [i for i in range(9)]
    board = [' ' for i in range(9)]
    depth_factor = 0.05
    while len(list_board) != 0:
        list_board, board = computer_move_x(list_board, board)
        board_positions.append(board)
        board_naming.append(''.join(board))
        if check_for_win(board, 'X'):
            
            points.extend([max((np.abs(1-np.mod(i,2)) - len(list_board)*depth_factor),0) for i in range(9-len(list_board))])
            
            player.extend(['X' if np.mod(i, 2) == 0 else 'O' for i in range(9-len(list_board))])
            return board_positions, board_naming, points, player
        elif check_for_draw(board):
            
            points.extend([0.5 - len(list_board)*depth_factor for i in range(9-len(list_board))])
            
            player.extend(['X' if np.mod(i, 2) == 0 else 'O' for i in range(9-len(list_board))])
            return board_positions, board_naming, points, player
        
        list_board, board = computer_move_o(list_board, board)
        board_positions.append(board)
        board_naming.append(''.join(board))
        if check_for_win(board, 'O'):
            
            points.extend([max((np.mod(i,2) - len(list_board)*depth_factor), 0) for i in range(9-len(list_board))])
            
            player.extend(['X' if np.mod(i, 2) == 0 else 'O' for i in range(9-len(list_board))])
            return board_positions, board_naming, points, player
        elif check_for_draw(board):
            
            points.extend([0.5 - len(list_board)*depth_factor for i in range(9-len(list_board))])
            
            player.extend(['X' if np.mod(i, 2) == 0 else 'O' for i in range(9-len(list_board))])
            return board_positions, board_naming, points, player
        
def computer_move_probabilistic(board, dataset, bot):
    board = board.copy()
    board_list = [value for key, value in board.items()]
    best_choice = 0
    for key,value in enumerate(board_list):
        if value == ' ':
            board_copy = board_list.copy()
            board_copy[key] = bot
            possible_move = ''.join(board_copy)
            win_rate = dataset[dataset.index == possible_move]['win_rate'].values[0]
            print(possible_move, ':', win_rate)
            
            if win_rate > best_choice:
                best_choice = win_rate
                print(best_choice)
                choose_position = key +1
            else:
                pass
            
        else:
            pass

    return choose_position

