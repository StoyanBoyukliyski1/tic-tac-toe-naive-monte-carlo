# -*- coding: utf-8 -*-
"""
Created on Wed Oct 27 14:48:28 2021

@author: Stoyan Boyukliyski
"""


import sys
import numpy as np

class TicTacToe():
    def __init__(self, board, player, bot):
        self.board = board
        self.player = player
        self.bot = bot
         
    def print_board(self):
        print(self.board[1] + '|' + self.board[2] + '|' + self.board[3])
        print('-+-+-')
        print(self.board[4] + '|' + self.board[5] + '|' + self.board[6])
        print('-+-+-')
        print(self.board[7] + '|' + self.board[8] + '|' + self.board[9])
        print("\n")
    
    def check_for_win(self, mark):
        if self.board[1] == self.board[2] and self.board[1] == self.board[3] and self.board[1] == mark:
            return True
        elif (self.board[4] == self.board[5] and self.board[4] == self.board[6] and self.board[4] == mark):
            return True
        elif (self.board[7] == self.board[8] and self.board[7] == self.board[9] and self.board[7] == mark):
            return True
        elif (self.board[1] == self.board[4] and self.board[1] == self.board[7] and self.board[1] == mark):
            return True
        elif (self.board[2] == self.board[5] and self.board[2] == self.board[8] and self.board[2] == mark):
            return True
        elif (self.board[3] == self.board[6] and self.board[3] == self.board[9] and self.board[3] == mark):
            return True
        elif (self.board[1] == self.board[5] and self.board[1] == self.board[9] and self.board[1] == mark):
            return True
        elif (self.board[7] == self.board[5] and self.board[7] == self.board[3] and self.board[7] == mark):
            return True
        else:
            return False
        
    def check_for_draw(self):
        for key, value in self.board.items():
            if value == ' ':
                return False
        return True
    
    def space_is_free(self, position):
        if self.board[position] == ' ':
            return True
        else:
            return False    
    
    def make_a_move(self, letter, position):
        if self.space_is_free(position):
            self.board[position] = letter
            self.print_board()
            if self.check_for_draw():
                print("Draw!")
                sys.exit()
            elif self.check_for_win(letter):
                if letter == 'X':
                    print("Xs have won")
                    sys.exit()
                else:
                    print('Os have won')
                    sys.exit()
                    
            return
        else:
            print("Can't insert there!")
            position = int(input("Please enter new position:  "))
            self.make_a_move(letter, position)
            return
    
    def player_move(self):
        position = int(input("Enter the position for your marker:  "))
        self.make_a_move(self.player, position)
        return

def priority_function():
    priority = input('Do you want to go first (Yes or No): ')
    if priority in ['Yes', 'No']:
        return priority
    else:
        print('Please enter again: ')
        priority = priority_function()
        return priority
    
def show_moves():
    print('These are the indexes for each cell of the board: ')
    print('1' + '|' + '2' + '|' + '3')
    print('-+-+-')
    print('4' + '|' + '5' + '|' + '6')
    print('-+-+-')
    print('7' + '|' + '8' + '|' + '9')
    print("\n")
    print('Have fun playing :)')